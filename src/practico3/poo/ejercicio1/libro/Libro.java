package practico3.poo.ejercicio1.libro;
/*
   * . Crear una clase llamada Libro que contenga los siguientes atributos:
        a) ISBN
        b) Título
        c) Autor
        d) Número de páginas

        La clase debe tener:
            e) Un constructor con todos los atributos pasados por parámetro
            f) Un constructor vacío
       --> Crear un método para cargar un libro pidiendo los datos al usuario y
        luego informar mediante otro método el número de ISBN, el título, el
        autor del libro y el número de páginas
   *
   * */
public class Libro {
   private String ISBN;


    public Libro(String isbn) {
        this.ISBN = isbn;
    }

    public void saludoLibro() {
        System.out.println("Soy Libro " + ISBN);

    }


}
